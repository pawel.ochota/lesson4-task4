const colorizeElement = require('..');
const { toHaveClass, toHaveAttribute, toHaveStyle } = require('@testing-library/jest-dom/matchers');

expect.extend({ toHaveClass, toHaveAttribute, toHaveStyle });

describe('colorizeElement', () => {
  it('should returns a HTMLElement', async () => {
    const result = colorizeElement(document.createElement('div'));

    expect(result instanceof HTMLElement).toBeTruthy();
  });

  it('should returns an element with .new-class class attribute', async () => {
    const div = document.createElement('div');
    const result = colorizeElement(div);

    expect(result).toHaveClass('new-class');
  });

  it('should returns an element with correct data-tag attribute', async () => {
    const div = document.createElement('div');
    const result = colorizeElement(div);
    const p = document.createElement('p');
    const result2 = colorizeElement(p);

    expect(result).toHaveAttribute('data-tag', 'div');
    expect(result2).toHaveAttribute('data-tag', 'p');
  });

  it('should returns an element with correct styles', async () => {
    const div = document.createElement('div');
    const result = colorizeElement(div);

    expect(result).toHaveStyle({
      fontSize: '12px',
      color: '#f1f1f1',
    });
  });
});
